<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 5/7/18
 * Time: 5:03 PM
 */

namespace Tests\Unit\DiscountService;


use App\Services\Discount\DiscountService;
use Tests\TestCase;

class DiscountServiceTest extends TestCase
{
    /** @test */
    public function it_can_init_discount_service()
    {
        $discountService = new DiscountService();
        $this->assertInstanceOf(DiscountService::class, $discountService);
    }

    /** @test */
    public function it_has_create_method()
    {
        $discountService = new DiscountService();
        $this->assertTrue(method_exists($discountService,'create'));
    }

    /** @test */
    public function it_has_is_valid_method()
    {
        $discountService = new DiscountService();
        $this->assertTrue(method_exists($discountService,'isValid'));
    }
}