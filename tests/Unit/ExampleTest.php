<?php

namespace Tests\Unit;

use App\Calculator;
use App\Calculator2;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
//        $this->assertTrue(true);

//        $calculator = new Calculator();
//        $this->assertEquals(5, $calculator->sum(2,3));

        $calculator = new Calculator2();
        $this->assertTrue(method_exists($calculator, 'sum'));
        $this->assertEquals(5, $calculator->sum(2,3));
    }
}
