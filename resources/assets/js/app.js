
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',

    data(){
        return {
            name: 'sample',
            attributeHtml:null
        };
    },

    methods:{
        getProductAttribute(category_id){
            axios.post('/admin/products/attributes/' + category_id, {}).then(response => {
                this.attributeHtml = response.data;
            }).catch(error => {
                console.log(error)
            });
        }
    }
});

$(document).ready(function ($) {
    $(document).on('click', '.updateCount', function (event) {
        event.preventDefault();
        var _this = $(this);
        var product_id = _this.data('id');
        var action = _this.data('action');
        var counter = $('#product-count-' + product_id);
        var counterValue = parseInt(counter.text());
        var basketTotalWrapper = $('#basket-total');
        var productSubTotalWrapper = $('#product-subTotal-' + product_id);
        axios.post('/basket/update', {
            product_id:product_id,
            action:action
        }).then(response => {
            if(response.data.success)
            {
                counterValue = action == 'plus' ? counterValue + 1 : counterValue - 1;
                counter.text(counterValue);
                basketTotalWrapper.text(response.data.total);
                productSubTotalWrapper.text(response.data.subTotal);
            } else {
                alert(response.data.message);
            }
        }).catch(error => {

        });
    });

    $(document).on('click', '.add_attribute', function (event) {
        event.preventDefault();
        var newAttributeWrapper = $('#new_attribute_wrapper').clone();
        newAttributeWrapper.css('display','block');
        newAttributeWrapper.insertBefore($('#save_product_attributes'));
    });
});