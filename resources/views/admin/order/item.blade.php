<tr>
    <td>{{ $order->order_id }}</td>
    <td>{{ $order->user->name }}</td>
    <td>{{ $order->order_total_amount }}</td>
    <td>{{ $order->order_discount }}</td>
    <td>{{ $order->order_payable_amount }}</td>
    <td>{{ $order->order_items->count() }}</td>
    <td>{!! $order->present()->orderStatusHtml !!}</td>
    <td>
        {!! $order->present()->operations !!}
    </td>
</tr>