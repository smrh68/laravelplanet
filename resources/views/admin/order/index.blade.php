@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{  $title }}</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover table-striped">
                        @include('admin.order.columns')
                        <tbody>
                        @if($orders && count($orders) > 0)
                            @foreach($orders as $order)
                                @include('admin.order.item')
                            @endforeach
                        @else
                            @include('admin.order.no-item')
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection