<tr>
    <td>{{ $attribute->attribute_name }}</td>
    <td>{{ $attribute->category->attribute_category_name }}</td>
    <td>{{ isset($attribute->type)? $attribute->type->attribute_type_name : '' }}</td>
    <td>{{ $attribute->is_searchable }}</td>
    <td>{{ $attribute->is_filterable }}</td>
    <td></td>
</tr>