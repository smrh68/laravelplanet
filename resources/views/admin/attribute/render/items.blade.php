<div class="product_attributes">
    @foreach($attributes as $category_title => $items)
        <p>{{ $category_title }}</p>
        @foreach($items as $item)
            @include('admin.attribute.render.' . $item->renderer)
        @endforeach
    @endforeach
</div>