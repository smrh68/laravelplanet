<div class="form-group">
    <label class="control-label" for="attribute-{{ $item->attribute_id }}">{{ $item->attribute_name }}</label>
    <input class="form-control"
           id="attribute-{{ $item->attribute_id }}"
           type="text"
           name="product_attributes[{{ $item->attribute_id }}]"
           value="{{ $product_attributes[$item->attribute_id]['value'] }}"
    >
</div>