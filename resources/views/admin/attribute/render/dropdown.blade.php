<div class="form-group">
    <label class="control-label" for="attribute-{{ $item->attribute_id }}">{{ $item->attribute_name }}</label>
    <select class="form-control" name="product_attributes[{{ $item->attribute_id }}]" id="attribute-{{ $item->attribute_id }}">
        @foreach($item->type->values as $valueItem)
            <option value="">{{ $valueItem->attribute_type_value }}</option>
        @endforeach
    </select>
</div>