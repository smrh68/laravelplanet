@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="header">{{ $title  }}</div>
                <div class="content">
                    @include('partials.errors')
                    @if(session('success'))
                        <div class="alert alert-success">
                            <p>ویژگی جدید با موفقیت ایجاد گردید.</p>
                        </div>
                        @endif
                    <form method="post" action="{{ route('admin.attribute.store') }}">
                        {{ csrf_field()  }}
                        <div class="form-group">
                            <label>عنوان ویژگی :</label>
                            <input
                                    type="text"
                                    name="attribute_name"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>دسته بندی :</label>
                            <select
                                    name="attribute_category_id"
                                    id="attribute_category_id"
                                    class="form-control">
                                <option value="0">بدون والد</option>
                                @if($categories && count($categories) > 0)
                                    @foreach($categories as $category)
                                        <option value="{{$category->attribute_category_id}}">{{$category->attribute_category_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>نوع ویژگی :</label>
                            <select
                                    name="attribute_type_id"
                                    id="attribute_type"
                                    class="form-control">
                                @if($contentType && count($contentType) > 0)
                                    @foreach($contentType as $contentTypeItem)
                                        <option value="{{$contentTypeItem->attribute_type_id}}">{{$contentTypeItem->attribute_type_name}}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>نوع نمایش :</label>
                            <select
                                    name="attribute_type"
                                    id="attribute_type"
                                    class="form-control">
                                @if($types && count($types) > 0)
                                    @foreach($types as $type_id => $type_name)
                                        <option value="{{$type_id}}">{{$type_name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>قابلیت جستجو :</label>
                            <input
                                    type="checkbox"
                                    name="is_searchable"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>قابلیت فیلتر :</label>
                            <input
                                    type="checkbox"
                                    name="is_filterable"
                                    class="form-control">
                        </div>

                        <button type="submit" class="btn btn-fill btn-info">ذخیره اطلاعات</button>
                    </form>
                </div>
            </div> <!-- end card -->

        </div>
    </div>
    @endsection