<li>
    <input
            name="product_category_id"
            type="radio"
            value="{{$category->category_id}}"
            @change="getProductAttribute({{$category->category_id}})"
    >
    <label for="checkbox{{$loop->index}}">
        {{$category->category_title}}
    </label>
    @if(isset($categories[$category->category_id]))
        @include('admin.category.category_tree', ['collection' => $categories[$category->category_id]])
        @endif
</li>