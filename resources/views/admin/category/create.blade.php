@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">{{ $title  }}</div>
                <div class="content">
                    @include('partials.errors')
                    @if(session('success'))
                        <div class="alert alert-success">
                            <p>دسته بندی جدید با موفقیت ایجاد گردید.</p>
                        </div>
                        @endif
                    <form method="post" action="{{ route('admin.category.store') }}">
                        {{ csrf_field()  }}
                        <div class="form-group">
                            <label>عنوان دسته بندی :</label>
                            <input
                                    type="text"
                                    name="category_title"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>نامک دسته بندی :</label>
                            <input
                                    type="text"
                                    name="category_slug"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>دسته بندی والد :</label>
                            <select
                                    name="category_parent_id"
                                    id="category_parent_id"
                                    class="form-control">
                                <option value="0">بدون والد</option>
                                @if($categories && count($categories) > 0)
                                    @foreach($categories as $category)
                                        <option value="{{$category->category_id}}">{{$category->category_title}}</option>
                                        @endforeach
                                    @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>ویژگی ها :</label>
                            @if($attributes && count($attributes) > 0)
                                @foreach($attributes as $attribute)
                                    <input type="checkbox" id="attribute-{{ $attribute->attribute_id }}" value="{{ $attribute->attribute_id }}" name="product_category_attributes[]">
                                    <label for="attribute-{{ $attribute->attribute_id }}">{{ $attribute->attribute_name }}</label>
                                    @endforeach
                                @endif
                        </div>

                        <button type="submit" class="btn btn-fill btn-info">ذخیره اطلاعات</button>
                    </form>
                </div>
            </div> <!-- end card -->
        </div>
    </div>
    @endsection