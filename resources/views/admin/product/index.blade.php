@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{  $title }}</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover table-striped">
                        @include('admin.product.columns')
                        <tbody>
                        @if($products && count($products) > 0)
                            @foreach($products as $product)
                                @include('admin.product.item')
                            @endforeach
                        @else
                            @include('admin.product.no-item')
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection