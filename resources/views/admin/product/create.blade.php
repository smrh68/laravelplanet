@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="header">{{ $title }}</div>
                <div class="content">
                    @include('partials.errors')
                    @if(session('success'))
                        <div class="alert alert-success">
                            <p>محصول جدید با موفقیت ایجاد گردید.</p>
                        </div>
                    @endif
                    <form method="post" action="{{ route('admin.products.store') }}" enctype="multipart/form-data">
                        {{ csrf_field()  }}
                        <div class="form-group">
                            <label>عنوان محصول :</label>
                            <input
                                    type="text"
                                    name="product_title"
                                    class="form-control">
                            @if($errors->has('product_title'))
                                <p>عنوان محصول الزامی می باشد.</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>قیمت محصول :</label>
                            <input
                                    type="text"
                                    name="product_price"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>موجودی محصول :</label>
                            <input
                                    type="text"
                                    name="product_stock"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>تخفیف محصول :</label>
                            <input
                                    type="text"
                                    name="product_discount"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>نوع محصول :</label>
                            <input type="radio" name="product_type" value="1">
                            فیزیکی
                            <input type="radio" name="product_type" value="2">
                            مجازی

                        </div>
                        <div class="form-group">
                            <label>تعداد کوپن محصول :</label>
                            <input
                                    type="text"
                                    name="product_coupon_count"
                                    class="form-control">
                        </div>
                        <div class="form-group">
                            <label>توضیحات محصول :</label>
                            <textarea name="product_description" class="form-control" id="" cols="30" rows="10"></textarea>

                        </div>
                        <div class="form-group">
                            <label>دسته بندی های محصول :</label>
                            @include('admin.category.category_tree', ['collection' => $categories[0]])
                        </div>
                        <div class="form-group">
                            <label>ویژگی ها :</label>
                            <div v-if="attributeHtml" v-html="attributeHtml"></div>
                        </div>
                        <div class="form-group">
                            <label>تصویر شاخص :</label>
                            <input name="product_thumbnail" id="product_thumbnail" type="file">
                        </div>
                        <div class="form-group">
                            <label>وضعیت محصول :</label>
                            <select name="product_status" id="product_status">
                                <option value="1">عادی</option>
                                <option value="2">پیش فروش</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <input  name="product_visible" id="product_visible" type="checkbox">
                                <label for="product_visible">
                                    نمایش محصول
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-fill btn-info">ذخیره اطلاعات</button>
                    </form>
                </div>
            </div> <!-- end card -->

        </div>
    </div>
@endsection