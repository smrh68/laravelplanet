@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{  $title }}</h4>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" class="add_attribute btn btn-info">اضافه کردن مشخصه</a>
                            <form action="" method="post">
                                {{ csrf_field() }}
                                @foreach($attributes as $category_title => $items)
                                    <div class="category-title">
                                        {{$category_title}}
                                    </div>
                                    <div class="attribute-items">
                                        @foreach($items as $item)
                                            @include('admin.attribute.render.text', $product_attributes)
                                            <?php unset($product_attributes[$item->attribute_id]) ?>
                                        @endforeach
                                        @foreach($product_attributes as $id => $item)
                                            <div class="form-group">
                                                <label class="control-label" for="attribute-{{ $id }}">{{ $item['name'] }}</label>
                                                <input class="form-control"
                                                       id="attribute-{{ $id }}"
                                                       type="text"
                                                       name="product_attributes[{{ $id }}]"
                                                       value="{{ $item['value'] }}"
                                                >
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                                <button id="save_product_attributes" class="btn btn-primary" type="submit">ذخیره اطلاعات</button>
                            </form>
                            <div class="form-group" id="new_attribute_wrapper" style="display: none;">
                                {{--<select name="new_attribute_category" id="">--}}
                                    {{--@foreach($categories as $id => $value)--}}
                                        {{--<option value="{{$id}}">{{$value}}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                                <input type="text" name="new_attribute_name[]" placeholder="نام مشخصه">
                                <input type="text" name="new_attribute_value[]" placeholder="مقدار مشخصه">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection