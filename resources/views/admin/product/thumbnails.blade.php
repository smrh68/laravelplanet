@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{  $title }}</h4>
                </div>
                <div class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="{{ route('admin.products.thumbnails.save',[$product->product_id]) }}" method="post"  enctype="multipart/form-data" >
                                {{ csrf_field() }}

                                <button id="save_product_thumbnails" class="btn btn-primary" type="submit">ذخیره اطلاعات</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection