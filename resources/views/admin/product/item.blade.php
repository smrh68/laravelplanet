<tr>
    <td>{{ $product->product_code  }}</td>
    <td>{{ $product->product_title  }}</td>
    <td>{{ $product->present()->product_price_in_hezar_toman }}</td>
    <td>{{ 0  }}</td>
    <td>{{ $product->product_stock  }}</td>
    <td>{{ $product->product_status  }}</td>
    <td>
        <a href="{{ route('admin.comments') . '?pid=' . $product->product_id }}">مشاهده دیدگاه ها</a>
        <a href="{{ route('admin.products.attributes.assign',['product_id' => $product->product_id]) }}">ویرایش  مشخصات</a>
    </td>
</tr>