@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="header">{{ $title  }}</div>
                <div class="content">
                    @include('partials.errors')
                    @if(session('success'))
                        <div class="alert alert-success">
                            <p>دسته بندی جدید با موفقیت ایجاد گردید.</p>
                        </div>
                        @endif
                    <form method="post" action="{{ route('admin.attribute.category.create') }}">
                        {{ csrf_field()  }}
                        <div class="form-group">
                            <label>عنوان دسته بندی :</label>
                            <input
                                    type="text"
                                    name="attribute_category_name"
                                    class="form-control">
                        </div>
                        <button type="submit" class="btn btn-fill btn-info">ذخیره اطلاعات</button>
                    </form>
                </div>
            </div> <!-- end card -->

        </div>
    </div>
    @endsection