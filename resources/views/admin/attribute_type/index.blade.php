@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{  $title }}</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover table-striped">
                        @include('admin.attribute_type.columns')
                        <tbody>
                        @if($types && count($types) > 0)
                            @foreach($types as $type)
                                @include('admin.attribute_type.item')
                            @endforeach
                        @else
                            @include('admin.attribute_type.no-item')
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection