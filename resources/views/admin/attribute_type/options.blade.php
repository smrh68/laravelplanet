@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="header">{{ $title  }}</div>
                <div class="content">
                    @include('partials.errors')
                    @if(session('success'))
                        <div class="alert alert-success">
                            <p>نوع جدید با موفقیت ایجاد گردید.</p>
                        </div>
                    @endif
                    <form method="post" action="{{ route('admin.attribute.type.options.save', [$type_id]) }}">
                        {{ csrf_field()  }}
                        <div class="form-group">
                            <label>مقدار نوع :</label>
                            <input
                                    type="text"
                                    name="attribute_type_value"
                                    class="form-control">
                        </div>
                        <button type="submit" class="btn btn-fill btn-info">ذخیره اطلاعات</button>
                    </form>
                </div>
            </div> <!-- end card -->

        </div>
    </div>
@endsection