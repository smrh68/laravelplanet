@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{  $title }}</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-hover table-striped">
                        @include('admin.comment.columns')
                        <tbody>
                        @if($comments && count($comments) > 0)
                            @foreach($comments as $comment)
                                @include('admin.comment.item')
                            @endforeach
                        @else
                            @include('admin.comment.no-item')
                        @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    @endsection