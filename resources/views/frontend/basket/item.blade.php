<tr>
    <td>{{$item['product']->product_title}}</td>
    <td>{{$item['product']->present()->product_price_in_hezar_toman}}</td>
    <td>
        <a data-id="{{ $item['product']->product_id }}" data-action="plus" href="" class="fa fa-plus updateCount"></a>
        <span id="product-count-{{$item['product']->product_id}}">{{$item['count']}}</span>
        <a data-id="{{ $item['product']->product_id }}" data-action="minus" href="" class="fa fa-minus updateCount"></a>
    </td>
    <td>
        <span id="product-subTotal-{{ $item['product']->product_id }}">{{ $item['product']->product_price * $item['count'] }}</span>
    </td>
    <td>
        <a href="{{ route('basket.remove', [$item['product']->product_id]) }}">X</a>
    </td>
</tr>