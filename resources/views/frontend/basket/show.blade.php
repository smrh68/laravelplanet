@extends('layouts.frontend')

@section('content')
    @include('partials.nav')
    <div id="header"></div>
    <nav id="top-nav">
    </nav>
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">سبد خرید</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>
                            <th>محصول</th>
                            <th>قیمت</th>
                            <th>تعداد</th>
                            <th>قیمت کل</th>
                            <th>عملیات</th>
                        </tr>
                        @foreach($basket as $product_id => $item)
                            @include('frontend.basket.item')
                        @endforeach
                    </table>
                    <div class="basket-details">
                        <p class="col-md-6 col-sm-12">
                            <a class="btn btn-primary" href="/basket/checkout">تکمیل سفارش</a>
                        </p>
                        <p class="col-md-6 col-sm-12">
                            <p class="pull-right">
                                <span>جمع سفارش:</span>
                                <span id="basket-total">{{ \App\Services\Basket\BasketService::total() }}</span>
                            </p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection