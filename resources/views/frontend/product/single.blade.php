@extends('layouts.frontend')
@section('content')
    @include('partials.nav')
    <div id="header">

    </div>
    <nav id="top-nav"></nav>
    <div class="container product-single">
        <div class="row">
            <div class="col-xs-12 col-md-4 col-lg-4 product-gallery">
                <div class="product-thumbnail">
                    <img src="{{ $product->product_thumbnail }}" alt="">
                </div>
            </div>
            <div class="col-xs-12 col-md-8 col-lg-8 product-info">
                <div class="product-title">
                    {{ $product->product_title }}
                </div>
                <div class="product-price">
                    {{ $product->present()->product_price_in_hezar_toman }}
                </div>
                <div id="add-to-basket">
                    <a class="btn btn-success" href="{{ route('basket.add',[$product->product_id]) }}">اضافه کردن به سبد
                        خرید</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="product-single-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                              data-toggle="tab">نقد و بررسی تخصصی</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">مشخصات
                            فنی</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">نظرات
                            کاربران</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home"></div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                            @foreach($product_attributes as $category => $items)
                                <div class="attribute-group">
                                    <h4>{{ $category?:"مشخصات کلی محصول" }}</h4>
                                    <table class="table table-hover">
                                        @each('frontend.product.attribute',$items,'item')
                                    </table>
                                </div>
                            @endforeach
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages"></div>
                </div>

            </div>
        </div>
    </div>
@endsection