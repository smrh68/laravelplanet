<div class="col-sm-12 col-lg-3">
    <div class="product" id="product-{{ $product->product_id }}">
        <div class="product_thumbnail">
            <img src="{{ $product->product_thumbnail }}" alt="">
        </div>
        <div class="product-title">
            <a href="{{ route('product.single',[$product->product_id,$product->product_slug]) }}">{{ $product->product_title }}</a>
        </div>
        <div class="product-price">
            {{ $product->present()->product_price_in_hezar_toman }}
        </div>
        <div class="product-details">
            <a class="btn btn-success btn-block" href="/basket/add/{{ $product->product_id }}">افزودن به سبد خرید</a>
        </div>
    </div>
</div>