@extends('layouts.frontend')

@section('content')
    @include('partials.nav')
    <div id="header"></div>
    <nav id="top-nav">
    </nav>
    <div class="container">
        <div class="row">
            @each('frontend.product.item', $products, 'product')
        </div>
    </div>
@endsection