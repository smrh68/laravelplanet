@extends('layouts.frontend')

@section('content')
    @include('partials.nav')
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">ثبت نام در سایت</div>
                    <div class="panel-body">
                        <form action="{{ route('auth.register') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="user_full_name">نام کامل</label>
                                <input type="text" class="form-control" name="user_full_name" id="user_full_name" placeholder="Full Name">
                            </div>
                            <div class="form-group">
                                <label for="user_email">ایمیل</label>
                                <input type="email" class="form-control" name="user_email" id="user_email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="user_password">کلمه عبور</label>
                                <input type="password" class="form-control" name="user_password" id="user_password" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-success">ثبت نام</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection