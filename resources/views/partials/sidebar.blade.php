<div class="sidebar" data-color="blue" data-image="">
    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            Ct
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            Creative Tim
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="info">
                <div class="photo">
                    <img src="/img/default-avatar.png" />
                </div>

                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
						<span>
							Tania Andrew
	                        <b class="caret"></b>
						</span>
                </a>

                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a href="#pablo">
                                <span class="sidebar-mini">MP</span>
                                <span class="sidebar-normal">My Profile</span>
                            </a>
                        </li>

                        <li>
                            <a href="#pablo">
                                <span class="sidebar-mini">EP</span>
                                <span class="sidebar-normal">Edit Profile</span>
                            </a>
                        </li>

                        <li>
                            <a href="#pablo">
                                <span class="sidebar-mini">S</span>
                                <span class="sidebar-normal">Settings</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="active">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="pe-7s-graph"></i>
                    <p>داشبورد</p>
                </a>
            </li>
            <li>
                <a data-toggle="collapse" href="#componentsExamples">
                    <i class="pe-7s-plugin"></i>
                    <p>محصولات
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="componentsExamples">
                    <ul class="nav">
                        <li>
                            <a href="{{ route('admin.products') }}">
                                {{--<span class="sidebar-mini">B</span>--}}
                                <span class="sidebar-normal">همه محصولات</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.products.create') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">افزودن محصول جدید</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.category.index') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">دسته بندی محصولات</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.category.create') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">دسته بندی جدید</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#attributes">
                    <i class="pe-7s-plugin"></i>
                    <p>ویژگی ها
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="attributes">
                    <ul class="nav">
                        <li>
                            <a href="{{ route('admin.attributes') }}">
                                {{--<span class="sidebar-mini">B</span>--}}
                                <span class="sidebar-normal">لیست ویژگی ها</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.attribute.create') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">افزودن ویژگی جدید</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.attribute.categories') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">لیست دسته بندی ویژگی ها</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.attribute.category.create') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">افزودن دسته بندی ویژگی ها</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.attribute.types') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">لیست نوع ویژگی ها</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.attribute.type.create') }}">
                                {{--<span class="sidebar-mini">GS</span>--}}
                                <span class="sidebar-normal">افزودن نوع ویژگی</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#comments">
                    <i class="pe-7s-plugin"></i>
                    <p>دیدگاه ها
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="comments">
                    <ul class="nav">
                        <li>
                            <a href="{{ route('admin.comments') }}">
                                {{--<span class="sidebar-mini">B</span>--}}
                                <span class="sidebar-normal">همه دیدگاه ها</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>