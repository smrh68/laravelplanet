<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('order_user_id')->after('id');
            $table->integer('order_payment_method')->after('order_user_id');
            $table->integer('order_total_amount')->after('order_payment_method');
            $table->integer('order_discount')->after('order_total_amount');
            $table->integer('order_payable_amount')->after('order_discount');
            $table->text('order_discount_description')->nullable()->after('order_payable_amount');
            $table->text('order_description')->nullable()->after('order_discount_description');
            $table->integer('order_shipping_method')->nullable()->after('order_description');
            $table->integer('order_shipping_amount')->default(0)->after('order_shipping_method');
            $table->integer('order_packaging_amount')->default(0)->after('order_shipping_amount');
            $table->integer('order_status')->after('order_packaging_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
