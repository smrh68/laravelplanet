<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/basket/add/{product_id}', 'BasketController@add')->name('basket.add');
    Route::get('/basket', 'BasketController@show')->name('basket');
    Route::get('/basket/checkout', 'BasketController@checkout')->name('basket.checkout');
    Route::get('/basket/remove/{product_id}', 'BasketController@remove')->name('basket.remove');
    Route::post('/basket/update', 'BasketController@update')->name('basket.update');

    Route::get('auth/login', 'AuthController@showLogin')->name('auth.login.show');
    Route::post('auth/login', 'AuthController@doLogin')->name('auth.login');
    Route::get('auth/register', 'AuthController@showRegister')->name('auth.register.show');
    Route::post('auth/register', 'AuthController@doRegister')->name('auth.register');
    Route::get('logout', 'AuthController@logout')->name('auth.logout');

    //products
    Route::get('product/{product_id}/{product_slug}','ProductsController@single')->name('product.single');

    Route::get('orders/update', 'OrdersController@update');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('products', 'ProductsController@index')->name('admin.products');
    Route::get('products/create', 'ProductsController@create')->name('admin.products.create');
    Route::post('products/store', 'ProductsController@store')->name('admin.products.store');
    Route::get('products/attributes/assign/{product_id}', 'ProductsController@assignAttributes')->name('admin.products.attributes.assign');
    Route::post('products/attributes/assign/{product_id}', 'ProductsController@storeAssignAttributes')->name('admin.products.attributes.assign.store');
    Route::get('products/attachment/{product_id}', 'ProductsController@showProductThumbnail')->name('admin.products.thumbnails');
    Route::post('products/attachment/{product_id}', 'ProductsController@storeProductThumbnail')->name('admin.products.thumbnails.store');
    Route::get('products/category', 'CategoriesController@index')->name('admin.category.index');
    Route::get('products/category/create', 'CategoriesController@create')->name('admin.category.create');
    Route::post('products/category/create', 'CategoriesController@store')->name('admin.category.store');


    Route::get('products/comments', 'CommentsController@index')->name('admin.comments');
    Route::get('products/attributes', 'AttributesController@index')->name('admin.attributes');
    Route::get('products/attributes/create', 'AttributesController@create')->name('admin.attribute.create');
    Route::post('products/attributes/create', 'AttributesController@store')->name('admin.attribute.store');

    Route::get('products/attributes/categories', 'AttributesController@category_index')->name('admin.attribute.categories');
    Route::get('products/attributes/categories/create', 'AttributesController@category_create')->name('admin.attribute.category.create');
    Route::post('products/attributes/categories/create', 'AttributesController@category_store')->name('admin.attribute.category.store');

    Route::get('products/attributes/types', 'AttributesController@type_index')->name('admin.attribute.types');
    Route::get('products/attributes/types/create', 'AttributesController@type_create')->name('admin.attribute.type.create');
    Route::post('products/attributes/types/create', 'AttributesController@type_store')->name('admin.attribute.type.store');
    Route::post('products/attributes/{pid}', 'AttributesController@getCategoryAttribute')->name('admin.attribute.ajax');
    Route::get('products/attributes/options/{type_id}', 'AttributesController@getTypeOptions')->name('admin.attribute.type.options');
    Route::post('products/attributes/options/{type_id}', 'AttributesController@saveTypeOptions')->name('admin.attribute.type.options.save');

    //orders
    Route::get('orders', 'OrdersController@index')->name('admin.orders');
    Route::get('orders/create', 'OrdersController@create')->name('admin.orders.create');
    Route::get('orders/pay/{order_code}', 'OrdersController@payment')->name('admin.orders.payment');
    Route::get('orders/approve/{order_id}', 'OrdersController@approve')->name('admin.orders.approve');
});

