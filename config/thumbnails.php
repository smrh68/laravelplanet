<?php
return [
    'types' => [
        'thumbnail-small' => ['width' => 150,'height' => 150],
        'thumbnail-medium' => ['width' => 220,'height' => 220],
        'thumbnail-large' => ['width' => 350,'height' => 350],
    ]
];