<?php
return [
    'path' => public_path('media'),
    'url' => '/media/',
    'whitelist' => [
        'image/jpg',
        'image/png',
        'image/gif',
    ],
];