var gulp = require('gulp');
var rtlCss = require('gulp-rtlcss');

gulp.task('default', function () {
    return gulp.src('./public/css/bootstrap.min.css')
        .pipe(rtlCss())
        .pipe(gulp.dest('./public/css/rtl'));
});