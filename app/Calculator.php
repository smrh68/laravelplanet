<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 5/3/18
 * Time: 7:56 PM
 */

namespace App;


class Calculator
{
    public function sum(int $a, int $b)
    {
        return $a + $b;
    }
}