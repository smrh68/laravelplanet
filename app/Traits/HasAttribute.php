<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/10/18
 * Time: 11:11 AM
 */

namespace App\Traits;


use App\Models\Attribute;

trait HasAttribute
{
    public function attributes()
    {
        return $this->morphToMany(Attribute::class,
            'attributable',
            'attributables',
            'attributable_id',
            'attribute_id'
        )->withPivot('attribute_value');
    }
}