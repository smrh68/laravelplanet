<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/22/18
 * Time: 2:54 AM
 */

namespace App\Traits;


use App\Models\Filters\QueryFilter;

trait Filterable
{
    public function scopeFilter($query, QueryFilter $filter)
    {
        return $filter->apply($query);
    }
}