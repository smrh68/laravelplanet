<?php

namespace App\Traits;


use App\Models\Attachment;

trait HasAttachment
{
    public function attachments()
    {
        return $this->morphToMany(Attachment::class,
            'attachable',
            'attachables',
            'attachable_id',
            'attachment_id');
    }
}