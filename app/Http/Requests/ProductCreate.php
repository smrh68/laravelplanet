<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_title' => 'required',
            'product_price' => 'required|integer',
            'product_stock' => 'required|integer',
            'product_discount' => 'integer',
            'product_type' => 'required|integer',
            'product_coupon_count' => 'integer',
            'product_description' => 'required',
            'product_status'      => 'required|integer',
            'product_visible'    => 'required'
        ];
    }

    public function messages()
    {
        return [
            'product_title.required' => 'عنوان محصول الزامی میباشد.',
            'product_price.required' => 'قیمت محصول الزامی میباشد.',
            'product_price.integer' => 'قیمت محصول باید به صورت عددی باشد.',
        ];
    }
}
