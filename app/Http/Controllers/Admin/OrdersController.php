<?php

namespace App\Http\Controllers\Admin;

use App\Events\OrderCreated;
use App\Jobs\SendInvoice;
use App\Models\Order\OrderStatus;
use App\Repositories\Order\OrderRepository;
use App\Services\Order\OrderService;
use App\Services\Payment\PaymentService;
use Illuminate\Http\Request;

class OrdersController extends BaseController
{
    private $orderService;
    public function __construct()
    {
        parent::__construct();
        $this->repository = new OrderRepository();
        $this->orderService = new OrderService();
    }

    public function index()
    {
        $orders = $this->repository->all(['user', 'order_items']);
        $title = 'لیست سفارش ها';
        return view('admin.order.index', compact('orders', 'title'));
    }

    public function create()
    {
        $orderService = new OrderService();
        $order = $orderService->create([
            'user_id' => 1,
            'total_amount' => 1500000,
            'order_discount' => 0,
            'order_items' => [
                [
                    'product_id' => 1,
                    'amount' => 400000,
                    'discount' => 0
                ],
                [
                    'product_id' => 2,
                    'amount' => 600000,
                    'discount' => 0
                ],
                [
                    'product_id' => 3,
                    'amount' => 500000,
                    'discount' => 0
                ],
            ]
        ]);
        event(new OrderCreated($order['order']));
//        SendInvoice::dispatch($order)
//            ->delay(now()->addMinutes(10))
//            ->onQueue('emails')
//            ->onConnection('sqs');
    }

    public function payment(Request $request, $order_code)
    {
        $orderItem = $this->repository->findBy(['order_code' => $order_code], ['user']);
        $paymentService = new PaymentService();
        $paymentService->pay($orderItem);
    }

    public function approve(Request $request, $order_id)
    {
        $orderItem = $this->repository->find($order_id);
        $orderItem->updateStatus(OrderStatus::PAID);
        $this->orderService->updateStatus($order_id, $orderItem->order_status, OrderStatus::PAID);
    }
}