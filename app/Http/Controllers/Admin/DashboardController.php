<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;

class DashboardController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ProductRepository();
    }
    public function index()
    {
        $title = 'فروش محصولات بر اساس کشور';
        return view('admin.dashboard.index', compact('title'));
    }
}
