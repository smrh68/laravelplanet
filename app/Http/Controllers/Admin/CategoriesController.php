<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Repositories\category\CategoryRepository;
use App\Repositories\Product\ProductAttributeRepository;
use Illuminate\Http\Request;

class CategoriesController extends BaseController
{
    private $attribute_repository;
    public function __construct()
    {
        parent::__construct();
        $this->repository = new CategoryRepository();
        $this->attribute_repository = new ProductAttributeRepository();
    }

    public function index()
    {
        $title = 'دسته بندی محصولات';
        $categories = $this->repository->all();
        return view('admin.category.index', compact('categories', 'title'));
    }

    public function create()
    {
        $title = 'ایجاد دسته بندی جدید';
        $categories = $this->repository->all();
        $attributes = $this->attribute_repository->all();
        return view('admin.category.create', compact('title', 'categories', 'attributes'));
    }

    public function store(Request $request)
    {
        $newCategoryData = [
            'category_title' => $request->input('category_title'),
            'category_slug' => $request->input('category_slug'),
            'category_parent_id' => $request->input('category_parent_id')
        ];

        $newCategory = $this->repository->create($newCategoryData);
        if ($newCategory && is_a($newCategory, Category::class)){
            $newCategory->attributes()->sync($request->input('product_category_attributes'));
            return redirect()->back()->with(['success' => true]);
        }
    }
}
