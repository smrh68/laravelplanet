<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductCreate;
use App\Models\Attachment\AttachmentType;
use App\Models\Attribute;
use App\Models\AttributeCategory;
use App\Models\Product;
use App\Repositories\Attachment\AttachmentRepository;
use App\Repositories\category\CategoryRepository;
use App\Repositories\Product\ProductRepository;
use App\Services\Upload\UploadService;
use Illuminate\Http\Request;

class ProductsController extends BaseController
{
    private $categoryRepository;
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ProductRepository();
        $this->categoryRepository = new CategoryRepository();
    }

    public function index(Request $request)
    {
        $title = 'لیست محصولات';
        $products = $this->repository->all();
        return view('admin.product.index', compact('title', 'products'));
    }

    public function create()
    {
        $title = 'ایجاد محصول جدید';
        $categories = (new CategoryRepository())->all()->groupBy('category_parent_id');
        return view('admin.product.create', compact('title', 'categories'));
    }

    public function store(ProductCreate $request)
    {
//        $request->validate([
//            'product_title' => 'required',
//            'product_price' => 'required|integer',
//            'product_stock' => 'required|integer',
//            'product_discount' => 'integer',
//            'product_type' => 'required|integer',
//            'product_coupon_count' => 'integer',
//            'product_description' => 'required',
//            'product_status'      => 'required|integer',
//            'product_visible'    => 'required'
//        ],[
//            //'product_title.required' => 'عنوان محصول الزامی میباشد.',
//            'product_price.required' => 'قیمت محصول الزامی میباشد.',
//            'product_price.integer' => 'قیمت محصول باید به صورت عددی باشد.',
//        ]);

        $newProduct = $this->repository->create([
            'product_code' => 0,
            'product_category_id' => $request->input('product_category_id'),
            'product_title' => $request->input('product_title'),
            'product_price' => $request->input('product_price'),
            'product_slug' => '',
            'product_stock' => $request->input('product_stock'),
            'product_discount' => $request->input('product_discount'),
            'product_type' => $request->input('product_type'),
            'product_coupon_count' => $request->input('product_coupon_count'),
            'product_description' => $request->input('product_description'),
            'product_status' => $request->input('product_status'),
            'product_visible' => $request->exists('product_visible') ? 1 : 0,
        ]);

        if ($newProduct && is_a($newProduct, Product::class))
        {
            $attributes = $request->input('product_attributes');
            $formattedAttributes = [];
            foreach ($attributes as $attribute_id => $attribute_value)
            {
                $formattedAttributes[$attribute_id] = ['attribute_value' => $attribute_value];
            }
            $newProduct->attributes()->sync($formattedAttributes);
//            $whiteList = config('upload.whitelist');
//            $uploadedFile = $request->file('product_thumbnail');
//            && in_array($uploadedFile->getClientMimeType(), $whiteList)
            if ($request->hasFile('product_thumbnail'))
            {
                $newFile = UploadService::image($request->product_thumbnail);
                $attachmentRepo = new AttachmentRepository();
                $createAttachment = $attachmentRepo->create([
                    'attachment_type' => AttachmentType::IMAGE,
                    'attachment_name' => $newFile['name'],
                    'attachment_size' => $newFile['size']
                ]);
                $newProduct->attachments()->sync([$createAttachment->attachment_id]);
            }
            return redirect()->back()->with('success', true);
        }
    }

    public function assignAttributes(Request $request, $product_id)
    {
        $title = 'ویرایش مشخصات محصول';
        $product = $this->repository->find($product_id, ['category', 'attributes']);
        $categories = AttributeCategory::get();
        $categories = $categories->pluck('attribute_category_name', 'attribute_category_id');
        $product_attributes = $product->attributes;
        $formatProductAttributes = [];
        foreach ($product_attributes as $attr)
        {
            $formatProductAttributes[$attr->attribute_id] = [
                'value' => $attr->pivot->attribute_value,
                'name' => $attr->attribute_name
            ];
        }
        $product_attributes = $formatProductAttributes;
        //$product_attributes = $product_attributes->pluck('pivot.attribute_value', 'attribute_id', 'attribute_name')->toArray();
        $category_id = $product->category->category_id;
        $attributes = $this->categoryRepository->getAttributesByCategoryId($category_id);
        $attributes = $attributes->groupBy('category.attribute_category_name');
        return view('admin.product.assign', compact('title', 'product', 'attributes', 'product_attributes', 'categories'));
    }

    public function storeAssignAttributes(Request $request, $product_id)
    {
        $product = $this->repository->find($product_id);
        $new_attribute_name = $request->input('new_attribute_name');
        $new_attribute_value = $request->input('new_attribute_value');
        $new_attributes = [];
        foreach ($new_attribute_name as $index => $item)
        {
            $new_attributes[$item] = $new_attribute_value[$index];
        }
        $createdAttributes = [];
        foreach ($new_attributes as $name => $value)
        {
            $createdAttributes[] = Attribute::create([
                'attribute_name' => $name,
                'attribute_category_id' => 0,
                'attribute_type' => 1,
                'attribute_type_id' => 0,
                'is_searchable' => 0,
                'is_filterable' => 0
            ]);
        }
        $attributes = $request->input('product_attributes');
        $formattedAttributes = [];
        if ($product && !empty($attributes)) {
            foreach ($attributes as $attribute_id => $attribute_value)
            {
                $formattedAttributes[$attribute_id] = ['attribute_value' => $attribute_value];
            }
            foreach ($createdAttributes as $newAttribute)
            {
                $formattedAttributes[$newAttribute->attribute_id] = [
                    'attribute_value' => $new_attributes[$newAttribute->attribute_name]
                ];
            }
            $result = $product->attributes()->sync($formattedAttributes);

            if (is_array($result))
            {
                return back()->with('success', true);
            }
        }
    }

    public function showProductThumbnails(Request $request,$product_id)
    {
        $product = $this->repository->find($product_id);
        $title = 'تنظیم تصاویر محصول';
        return view('admin.product.thumbnails',compact('product','title'));
    }
}
