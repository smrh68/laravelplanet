<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Comment\CommentsRepositories;
use Illuminate\Http\Request;

class CommentsController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new CommentsRepositories();
    }

    public function index(Request $request)
    {
        $title = 'لیست دیدگاه ها';
        $comments = $this->repository->all();
        if ($request->has('pid') && intval($request->input('pid')))
        {
            $comments = $this->repository->commentsByModel($request->input('pid'));
        }
        return view('admin.comment.index', compact('title', 'comments'));
    }
}
