<?php

namespace App\Http\Controllers\Admin;

use App\Models\Attribute\AttributeTypes;
use App\Repositories\Attribute\AttributeTypeRepository;
use App\Repositories\Attribute\AttributeTypeValueRepository;
use App\Repositories\category\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductAttributeRepository;

class AttributesController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ProductAttributeRepository();
    }

    public function index()
    {
        $title = 'لیست ویژگی ها';
        $attributes = $this->repository->all(['category']);
        return view('admin.attribute.index', compact('attributes', 'title'));
    }

    public function create()
    {
        $attributeTypeRepo = new AttributeTypeRepository();
        $title = 'ایجاد ویژگی';
        $categories = $this->repository->getAllCategories();
        $types = AttributeTypes::getTypes();//$this->repository->getAllTypes();
        $contentType = $attributeTypeRepo->all();
        return view('admin.attribute.create', compact('title', 'categories', 'types', 'contentType'));
    }

    public function store(Request $request)
    {
        $newAttribute = $this->repository->create([
            'attribute_name' => $request->input('attribute_name'),
            'attribute_category_id' => $request->input('attribute_category_id'),
            'attribute_type' => $request->input('attribute_type'),
            'is_searchable' => $request->has('is_searchable'),
            'is_filterable' => $request->has('is_filterable')
        ]);
        if ($newAttribute)
        {
            return redirect()->back()->with('success', true);
        }
    }

    public function category_index()
    {
        $categories = $this->repository->getAllCategories();
        $title = 'لیست دسته بندی های ویژگی ها';
        return view('admin.attribute_category.index', compact('title', 'categories'));
    }

    public function category_create()
    {
        $title = 'ایجاد دسته بندی ویژگی';
        return view('admin.attribute_category.create', compact('title'));
    }

    public function category_store(Request $request)
    {
        $this->repository->saveCategory([
            'attribute_category_name' => $request->input('attribute_category_name')
        ]);
    }

    public function type_index()
    {
        $title = 'لیست نوع ویژگی ها';
        $types = $this->repository->getAllTypes();
        return view('admin.attribute_type.index', compact('title', 'types'));
    }

    public function type_create()
    {
        $title = 'ایجاد نوع ویژگی';
        return view('admin.attribute_type.create', compact('title'));
    }

    public function type_store(Request $request)
    {
        $newType = $this->repository->saveType([
            'attribute_type_name' => $request->input('attribute_type_name')
        ]);
        if ($newType)
        {
            return redirect()->back()->with('success', true);
        }
    }

    public function getCategoryAttribute(Request $request, $pid)
    {
        $categoryRepository = new CategoryRepository();
        $attributes = $categoryRepository->getAttributesByCategoryId($pid);
        $attributes = $attributes->groupBy('category.attribute_category_name');
        $attributesHtml = view('admin.attribute.render.items', compact('attributes'))->render();
        return $attributesHtml;
    }

    public function getTypeOptions($type_id)
    {
        $title = 'ایجاد مقدار نوع';
        return view('admin.attribute_type.options', compact('title', 'type_id'));
    }

    public function saveTypeOptions(Request $request, $type_id)
    {
        $attributeTypeValueRepo = new AttributeTypeValueRepository();
        $result = $attributeTypeValueRepo->create([
            'attribute_type_id' => $type_id,
            'attribute_type_value' => $request->input('attribute_type_value')
        ]);
        if ($result){
            return redirect()->back()->with('success', true);
        }
    }
}
