<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    private $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    public function index()
    {
        return $this->productRepository->all();
    }
}
