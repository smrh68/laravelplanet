<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use MiladRahimi\LaraJwt\Facades\JwtAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::guard('api')->attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]))
        {
            $user = Auth::guard('api')->user();
            $token = JwtAuth::generateToken($user);
            return [
                'success' => true,
                'token' => $token
            ];
        }
        return [
            'success' => false,
            'message' => 'invalid information'
        ];
    }
}
