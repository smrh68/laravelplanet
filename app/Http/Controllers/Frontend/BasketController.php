<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\Product\ProductRepository;
use App\Services\Basket\BasketService;
use App\Services\Order\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasketController extends Controller
{
    public function add(Request $request, $product_id)
    {
        BasketService::add($product_id);
        return redirect()->back();
    }

    public function show()
    {
        $page_title = 'نمایش سبد خرید';
        $basket = BasketService::items();
        return view('frontend.basket.show', compact('page_title', 'basket'));
    }

    public function checkout()
    {
        $basket = session()->get('basket.items');
        $orderData = [
            'user_id' => 1, //Auth::user()->id
            'total_amount' => 0,
            'order_discount' => 0,
            'order_items' => [
            ]
        ];
        $total_amount = 0;
        foreach ($basket as $key => $item)
        {
            foreach ($item as $itemKey => $itemValue)
            {
                $orderData['order_items'][] = [
                    'product_id' => $itemKey,
                    'amount' => $itemValue['product']->product_price,
                    'discount' => 0
                ];
                $total_amount += $itemValue['product']->product_price * $itemValue['count'];
            }
        }
        $orderData['total_amount'] = $total_amount;
        $orderService = new OrderService();
        $orderService->create($orderData);
    }

    public function remove(Request $request, $product_id)
    {
        BasketService::remove($product_id);
        return redirect()->back()->with(['success' => true]);
    }

    public function update(Request $request)
    {
        $product_id = $request->input('product_id');
        $action = $request->input('action');
        $result = BasketService::update($product_id, $action);
        if ($result['success'])
        {
            $result['subTotal'] = BasketService::subTotal($product_id);
            $result['total'] = BasketService::total();
        }
        return $result;
    }
}
