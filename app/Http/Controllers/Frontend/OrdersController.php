<?php

namespace App\Http\Controllers\Frontend;

use App\Services\Order\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function update()
    {
        $orderService = new OrderService();
        $orderService->updateStatus(15, 1, 3);
    }
}
