<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/18/18
 * Time: 6:47 AM
 */

namespace App\Http\Controllers\Frontend;


use App\Repositories\Product\ProductRepository;

class ProductsController
{
    private $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }
    public function single($product_id,$product_slug)
    {

        $page_title = 'نمایش جزئیات محصول';
        $product = $this->productRepository->find($product_id,['attributes']);
        $product_attributes= $product->attributes->groupBy('category.attribute_category_name');
        return view('frontend.product.single',compact('page_title','product','product_attributes'));
    }
}