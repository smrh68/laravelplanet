<?php

namespace App\Http\Controllers\Frontend;

use App\Services\User\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLogin()
    {
        $page_title = 'ورود به سایت';
        return view('auth.login', compact('page_title'));
    }

    public function doLogin(Request $request)
    {
        //validation

        if (Auth::attempt([
            'email' => $request->input('user_email'),
            'password' => $request->input('user_password')
        ])){
            return redirect()->route('home');
        }
    }

    public function showRegister()
    {
        $page_title = 'ثبت نام';
        return view('auth.register', compact('page_title'));
    }

    public function doRegister(Request $request)
    {
        //validation

        UserService::create($request->except('_token'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
