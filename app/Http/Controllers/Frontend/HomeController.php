<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Filters\ProductFilters;
use App\Models\Product;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    private $productRepository;
    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }
    public function index(ProductFilters $filters)
    {
        $page_title = 'فروشگاه دنیای لاراول';
        $products = Cache::remember('home-page-products', 60, function () use ($filters){
            return Product::filter($filters)->get();
        });

//        $products = Product::filter($filters)->get();

//        $query = Product::query();
//        if ($request->has('minPrice'))
//        {
//            $query->where('product_price', '>=', $request->input('minPrice'));
//        }
//        if ($request->has('maxPrice'))
//        {
//            $query->where('product_price', '<=', $request->input('maxPrice'));
//        }
//        $products = $query->get(); //$this->productRepository->all();
        return view('frontend.home', compact('page_title', 'products'));
    }
}
