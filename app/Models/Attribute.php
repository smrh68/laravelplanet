<?php

namespace App\Models;

use App\Models\Attribute\AttributeTypes;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $primaryKey = 'attribute_id';

    protected $guarded = ['attribute_id'];

    public function category()
    {
        return $this->belongsTo(AttributeCategory::class, 'attribute_category_id');
    }

    public function type()
    {
        return $this->belongsTo(AttributeType::class, 'attribute_type_id');
    }

    public function product_categories()
    {
        return $this->morphedByMany(Category::class,
            'attributable',
            'attributables',
            'attributable_id',
            'attribute_id'
        );
    }

    public function getRendererAttribute()
    {
        return AttributeTypes::getBladeByType($this->attributes['attribute_type']);
    }
}
