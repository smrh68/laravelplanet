<?php

namespace App\Models;

use App\Presenters\CategoryPresenter;
use App\Presenters\Contract\Presentable;
use App\Traits\HasAttribute;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Presentable, HasAttribute;

    protected $primaryKey = 'category_id';

    protected $guarded = [];

    public $timestamps = false;

    protected $presenter = CategoryPresenter::class;

//    const CREATED_AT = '';
//    const UPDATED_AT = '';

    public function parent()
    {
        return $this->belongsTo(Category::class, 'category_parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'product_category_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'category_parent_id');
    }
}
