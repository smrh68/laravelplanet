<?php

namespace App\Models;

use App\Presenters\Contract\Presentable;
use App\Presenters\Currency;
use App\Presenters\ProductPresenter;
use App\Traits\Filterable;
use App\Traits\HasAttachment;
use App\Traits\HasAttribute;
use App\Traits\HasComment;
use App\Traits\HasTag;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Presentable, HasComment, HasTag, HasAttribute, HasAttachment, Filterable;

    protected $primaryKey = 'product_id';

//    protected $fillable = [];
    protected $guarded = ['product_id'];

    protected $presenter = ProductPresenter::class;

//    public function getProductPriceAttribute()
//    {
//        return Currency::toman($this->attributes['product_price']);
//    }

    public function setProductSlugAttribute()
    {
        $this->attributes['product_slug'] = preg_replace('/\s+/','-', $this->attributes['product_title']);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'product_category_id');
    }

    public function getProductThumbnailAttribute()
    {
        $thumbnail = $this->attachments->first();
        return config('upload.url') . optional($thumbnail)->attachment_name;
    }

    public function thumbnail(string $name)
    {
        $thumbnail = config('thumbnails.types');
        $size = $thumbnail['thumbnail-'.$name];
        $thumbnail = $this->attachments->first();
        list($file,$extension) = explode('.',optional($thumbnail)->attachment_name);
        return config('upload.url').$file.'-'.$size['width'].'x'.$size['height'].'.'.$extension;
    }
}
