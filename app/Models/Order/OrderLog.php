<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    protected $primaryKey = 'order_log_id';
    protected $guarded = ['order_log_id'];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_log_order_id');
    }
}
