<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/17/18
 * Time: 10:03 PM
 */

namespace App\Models\Attachment;


class AttachmentType
{
    const IMAGE = 1;
    const DOCUMENT = 2;
    const VIDEO = 3;
    const AUDIO = 4;
}