<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/22/18
 * Time: 2:13 AM
 */

namespace App\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilter
{
    protected $request;

    protected $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply(Builder $builder)
    {
        $this->builder = $builder;
        foreach ($this->filters() as $name => $value)
        {
            if (!method_exists($this, $name))
            {
                continue;
            }
            !empty($value) & $value > 0 ? $this->{$name}($value) : $this->{$name}();
        }
        return $this->builder;
    }

    public function filters()
    {
        return $this->request->all();
    }
}