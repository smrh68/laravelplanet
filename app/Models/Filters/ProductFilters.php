<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/22/18
 * Time: 2:31 AM
 */

namespace App\Models\Filters;


class ProductFilters extends QueryFilter
{
    public function minPrice($value = null)
    {
        return $this->builder->where('product_price', '>=', $value);
    }

    public function maxPrice($value = null)
    {
        return $this->builder->where('product_price', '<=', $value);
    }

    public function sort($value = null)
    {
        $order = $this->request->input('order');
        return $this->builder->orderBy('product_price', strtoupper($order));
    }
}