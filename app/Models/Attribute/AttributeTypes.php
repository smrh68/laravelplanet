<?php

namespace App\Models\Attribute;


class AttributeTypes
{
    const TEXT = 1;
    const DROP_DOWN = 2;
    const RADIO = 3;
    const CHECKBOX = 4;
    const TEXT_AREA = 5;

    public static function getTypes()
    {
        return [
            self::TEXT => 'متنی',
            self::DROP_DOWN => 'کشویی',
            self::RADIO => 'یک گزینه ای',
            self::CHECKBOX => 'چند گزینه ای',
            self::TEXT_AREA => 'متنی بزرگ',
        ];
    }

    public static function getBlades()
    {
        return [
            self::TEXT => 'text',
            self::DROP_DOWN => 'dropdown',
            self::RADIO => 'radio',
            self::CHECKBOX => 'checkbox',
            self::TEXT_AREA => 'textarea',
        ];
    }

    public static function getBladeByType(int $type)
    {
        $blades = self::getBlades();
        return $blades[$type];
    }
}