<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/17/18
 * Time: 11:53 AM
 */

namespace App\Services\Upload;


use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class UploadService
{
    public static function image(UploadedFile $file, $width = 250, $height = 300)
    {
        $fileExtension = $file->getClientOriginalExtension();
        $newName = str_random(10);
        $newFileName = $newName . '.' . $fileExtension;
        $newFile = $file->move(config('upload.path'), $newFileName);
        Image::make($newFile->getRealPath())->resize($width, $height)->save();
        return [
            'size' => $newFile->getSize(),
            'name' => $newFileName
        ];
    }
}