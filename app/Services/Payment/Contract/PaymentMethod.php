<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/19/18
 * Time: 7:40 PM
 */

namespace App\Services\Payment\Contract;


use App\Repositories\Order\OrderRepository;

abstract class PaymentMethod
{
    protected $orderRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
    }

    abstract public function pay(int $order_id);
}