<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/24/18
 * Time: 12:47 AM
 */

namespace App\Services\Payment;


use App\Services\Payment\PaymentMethods\Account;
use App\Services\Payment\PaymentMethods\Cod;
use App\Services\Payment\PaymentMethods\Online;

class PaymentType
{
    const ONLINE = 1;
    const COD = 2;
    const ACCOUNT = 3;

    public static function getPaymentTypes()
    {
        return [
            self::ONLINE => 'درگاه های آنلاین',
            self::ACCOUNT => 'حساب کاربری',
            self::COD => 'پرداخت در محل'
        ];
    }

    public static function getPaymentClass(int $type)
    {
        return [
            self::ONLINE => Online::class,
            self::COD => Cod::class,
            self::ACCOUNT => Account::class
        ][$type];
    }
}