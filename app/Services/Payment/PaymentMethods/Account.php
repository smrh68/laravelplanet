<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/19/18
 * Time: 11:48 PM
 */

namespace App\Services\Payment\PaymentMethods;


use App\Models\Order\OrderStatus;
use App\Services\Order\OrderService;
use App\Services\Payment\Contract\PaymentMethod;

class Account extends PaymentMethod
{

    public function pay(int $order_id)
    {
        $orderItem = $this->orderRepository->find($order_id);
        $orderOwner = $orderItem->user;
        if ($orderOwner->balance > $orderItem->order_total_amount)
        {
            $orderItem->order_status = OrderStatus::PAID;
            $orderItem->save();
            $orderOwner->decrement('balance', $orderItem->order_total_amount);
            $orderService = new OrderService();
            $updateResult = $orderService->updateStatus($order_id, OrderStatus::UNPAID, OrderStatus::PAID);
            if ($updateResult)
            {
                return [
                    'status' => true,
                    'order_id' => $order_id
                ];
            }
            return false;
        }
    }
}