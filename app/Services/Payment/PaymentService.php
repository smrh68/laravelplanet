<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/24/18
 * Time: 12:44 AM
 */

namespace App\Services\Payment;


use App\Models\Order\Order;

class PaymentService
{
    public function __construct()
    {

    }

    public function pay(Order $order)
    {
        $paymentClass = PaymentType::getPaymentClass($order->order_payment_method);
        $paymentClassInstance = new $paymentClass();
        $paymentResult = $paymentClassInstance->pay($order->order_id);
        dd($paymentResult);
    }
}