<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 5/7/18
 * Time: 4:33 PM
 */

namespace App\Services\Discount\Utility;


class DiscountCalculator
{
    public static function calculateByPercent(int $amount, int $percent)
    {
        if ($percent == 0)
        {
            return $amount;
        }
        return (1 - ($percent / 100)) * $amount;
    }

    public static function calculateByValue(int $amount, int $value)
    {
        return $amount - $value;
    }
}