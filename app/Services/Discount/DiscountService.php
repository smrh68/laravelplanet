<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 5/7/18
 * Time: 5:45 PM
 */

namespace App\Services\Discount;


use App\Models\Discount;
use App\Services\Discount\Exception\InvalidDateRangeException;

class DiscountService
{
    public function create(array $params)
    {
        if ($params['discount_start_date']->gt($params['discount_end_date']))
        {
            throw new InvalidDateRangeException('تاریخ های تخفیف نامعتبر است.');
        }
        $newDiscount = Discount::create($params);
        return $newDiscount;
    }

    public function isValid()
    {

    }
}