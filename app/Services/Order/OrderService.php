<?php

namespace App\Services\Order;


use App\Events\OrderCreated;
use App\Models\Order\OrderLog;
use App\Models\Order\OrderStatus;
use App\Repositories\Order\OrderItemRepository;
use App\Repositories\Order\OrderLogRepository;
use App\Repositories\Order\OrderRepository;
use App\Services\Discount\DiscountService;
use App\Services\Order\UpdateStatus\CheckAgent;
use App\Services\Order\UpdateStatus\CheckOrder;
use App\Services\Order\UpdateStatus\CheckStatus;
use App\Services\Payment\PaymentType;
use Illuminate\Support\Facades\Auth;

class OrderService
{
    private $orderRepository;
    private $orderItemRepository;
    private $orderLogRepository;

    public function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->orderItemRepository = new OrderItemRepository();
        $this->orderLogRepository = new OrderLogRepository();
    }

    public function create(array $data)
    {
        $newOrder = $this->orderRepository->create([
            'order_code' => str_random(10),
            'order_user_id' => $data['user_id'],
            'order_payment_method' => PaymentType::ACCOUNT,
            'order_total_amount' => $data['total_amount'],
            'order_discount' => $data['order_discount'],
            'order_payable_amount' => DiscountService::calculateByPercent($data['total_amount'], $data['order_discount']),
            'order_status' => OrderStatus::UNPAID
        ]);
        $orderItems = [];
        foreach ($data['order_items'] as $order_item)
        {
            $orderItems[] = [
                    'order_item_order_id' => $newOrder->id,
                    'order_item_product_id' => $order_item['product_id'],
                    'order_item_amount' => $order_item['amount'],
                    'order_item_product_discount' => $order_item['discount'],
                    'order_item_count' => 1,
                    'order_item_payable_amount' => DiscountService::calculateByPercent($order_item['amount'], $order_item['discount']),
            ];
        }
        $createdOrderItems = $newOrder->order_items()->createMany($orderItems);
        if ($createdOrderItems)
        {
            event(new OrderCreated($newOrder));
            return [
                'success' => true,
                'order' => $newOrder,
                'orderItems' => $createdOrderItems
            ];
        }
    }

    public function updateStatus(int $orderId, int $currentStatus, int $nextStatus, string $description = null)
    {
        $data = [
            'order_id' => $orderId,
            'current_status' => $currentStatus,
            'next_status' => $nextStatus,
            'description' => $description
        ];
        try{
            $checkAgent = new CheckAgent();
            $checkOrder = new CheckOrder($checkAgent);
            $checkStatus = new CheckStatus($checkOrder);
            $checkStatus->handle($data);
        }
        catch (\Exception $exception){
            dd($exception->getMessage());
        }
        $currentUser = Auth::user()->id;

        $result = $this->orderLogRepository->create([
            'order_log_order_id' => $orderId,
            'order_log_current_status' => $currentStatus,
            'order_log_next_status' => $nextStatus,
            'order_log_agent' => $currentUser,
            'order_log_description' => $description,
        ]);
        return $result instanceof OrderLog;
    }
}