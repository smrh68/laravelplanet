<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/22/18
 * Time: 4:54 PM
 */

namespace App\Services\Order\UpdateStatus;


use App\Models\Order\OrderStatus;
use Illuminate\Support\Facades\Auth;

class CheckAgent extends StatusHandler
{

    public function process(array $data)
    {
        $currentUserRole = 'admin';//Auth::user();
        if ($data['next_status'] == OrderStatus::REFUNDED & $currentUserRole != 'admin')
        {
            throw new \Exception('you are not allowed');
        }
        return true;
    }
}