<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/22/18
 * Time: 5:53 PM
 */

namespace App\Services\Order\UpdateStatus;


use App\Models\Order\OrderStatus;

class CheckStatus extends StatusHandler
{

    public function process(array $data)
    {
        if ($data['current_status'] == OrderStatus::UNPAID && $data['next_status'] == OrderStatus::REFUNDED)
        {
            throw new \Exception('invalid operation');
        }
        return true;
    }
}