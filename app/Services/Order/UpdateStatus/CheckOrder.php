<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/22/18
 * Time: 4:37 PM
 */

namespace App\Services\Order\UpdateStatus;

use App\Models\Order\Order;
use App\Repositories\Order\OrderRepository;

class CheckOrder extends StatusHandler
{

    public function process(array $data)
    {
        $orderRepository = new OrderRepository();
        $orderItem = $orderRepository->find($data['order_id']);
        if ($orderItem && $orderItem instanceof Order)
        {
            return true;
        }
        throw new \Exception('invalid order');
    }
}