<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/22/18
 * Time: 4:23 PM
 */

namespace App\Services\Order\UpdateStatus;


abstract class StatusHandler
{
    protected $nextHandler;

    public function __construct(StatusHandler $handler=null)
    {
        $this->nextHandler = $handler;
    }

    public function handle(array $data)
    {
        $result = $this->process($data);
        if ($result)
        {
            if ($this->nextHandler)
            {
                return $this->nextHandler->handle($data);
            }
        }
        return $result;
    }

    abstract public function process(array $data);
}