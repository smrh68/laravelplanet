<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 3/12/18
 * Time: 8:57 AM
 */

namespace App\Services\Basket;


use App\Repositories\Product\ProductRepository;

class BasketService
{
    public static function add(int $product_id)
    {
        $productRepository = new ProductRepository();
        $product = $productRepository->find($product_id);
        if ($product)
        {
            $items = self::items();
            if (is_null($items))
            {
                $items = [];
            }
            if (isset($items[$product_id]))
            {
                $items[$product_id]['count'] +=1;
            }else {
                $items[$product_id] = [
                    'count' => 1,
                    'product' => $product
                ];
            }
            session(['basket.items' => $items]);
        }
    }

    public static function items()
    {
        return session('basket.items');
    }

    public static function remove($product_id)
    {
        return session()->remove('basket.items.' . $product_id);
    }

    public static function total()
    {
        return collect(self::items())->reduce(function ($total, $item){
            return $total + ($item['product']->product_price * $item['count']);
        }, 0);
    }

    public static function update($product_id, $action)
    {
        $items = self::items();
        if (isset($items[$product_id]))
        {
            $product = $items[$product_id]['product'];
            $product = $product->fresh();
            $currentCount = $items[$product_id]['count'];
            if ($action == 'plus')
            {
                if ($currentCount < $product->product_stock)
                {
                    $items[$product_id]['count'] +=1;
                    $items[$product_id]['product'] = $product;
                    session(['basket.items' => $items]);
                }else{
                    return [
                        'success' => false,
                        'message' => 'حداکثر موجودی کالا'
                    ];
                }
            }
            if ($action == 'minus')
            {
                if ($items[$product_id]['count'] > 1)
                {
                    $items[$product_id]['count'] -=1;
                    session(['basket.items' => $items]);
                }else{
                    return [
                        'success' => false,
                        'message' => 'حداقل یک آیتم الزامیست'
                    ];
                }
            }
            return ['success' => true];
        }
        return [
            'success' => false,
            'message' => 'محصول ناموجود'
        ];
    }

    public static function subTotal(int $product_id)
    {
        $items = self::items();
        if (isset($items[$product_id]))
        {
            return $items[$product_id]['count'] * $items[$product_id]['product']->product_price;
        }
        return 0;
    }
}