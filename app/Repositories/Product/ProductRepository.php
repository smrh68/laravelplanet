<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 1/26/18
 * Time: 2:00 PM
 */

namespace App\Repositories\Product;


use App\Models\Product;
use App\Repositories\Contract\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Product::class;
    }
}