<?php


namespace App\Repositories\category;


use App\Models\Category;
use App\Repositories\Contract\BaseRepository;

class CategoryRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Category::class;
    }

    public function getAttributesByCategoryId(int $category_id)
    {
        return $this->model::find($category_id)->attributes;
    }
}