<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/17/18
 * Time: 9:56 PM
 */

namespace App\Repositories\Attachment;


use App\Models\Attachment;
use App\Repositories\Contract\BaseRepository;

class AttachmentRepository extends BaseRepository
{
    protected $model = Attachment::class;
}