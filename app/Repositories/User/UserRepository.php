<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 4/12/18
 * Time: 4:08 PM
 */

namespace App\Repositories\User;


use App\Models\User;
use App\Repositories\Contract\BaseRepository;

class UserRepository extends BaseRepository
{
    protected $model = User::class;
}