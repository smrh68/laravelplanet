<?php

namespace App\Repositories\Order;


use App\Models\Order\OrderLog;
use App\Repositories\Contract\BaseRepository;

class OrderLogRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = OrderLog::class;
    }
}