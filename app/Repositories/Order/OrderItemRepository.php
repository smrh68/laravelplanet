<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/16/18
 * Time: 2:43 PM
 */

namespace App\Repositories\Order;


use App\Models\Order\OrderItem;
use App\Repositories\Contract\BaseRepository;

class OrderItemRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = OrderItem::class;
    }
}