<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/16/18
 * Time: 2:42 PM
 */

namespace App\Repositories\Order;


use App\Models\Order\Order;
use App\Repositories\Contract\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Order::class;
    }
}