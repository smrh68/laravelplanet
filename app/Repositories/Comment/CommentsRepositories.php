<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/2/18
 * Time: 8:03 PM
 */

namespace App\Repositories\Comment;


use App\Models\Comment;
use App\Models\Product;
use App\Repositories\Contract\BaseRepository;

class CommentsRepositories extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Comment::class;
    }

    public function commentsByModel(int $model_id, string $model_type = Product::class)
    {
        $model_object = $model_type::find($model_id);
        return $model_object->comments;
    }
}