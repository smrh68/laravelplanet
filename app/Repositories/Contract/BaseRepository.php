<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 1/26/18
 * Time: 2:00 PM
 */

namespace App\Repositories\Contract;


abstract class BaseRepository
{
    protected $model;

    public function __construct()
    {

    }

    public function find(int $id,array $relations=null)
    {
        if(!is_null($relations))
        {
            return $this->model::with($relations)->find($id);
        }
        return $this->model::find($id);
    }

    public function all(array $eagerLoadItems = [])
    {
        if (!empty($eagerLoadItems))
        {
            return $this->model::with($eagerLoadItems)->get();
        }
        return $this->model::all();
    }

    public function create(array $data)
    {
        return $this->model::create($data);
    }

    public function findBy(array $criteria, array $relations = null, $single = true)
    {
        $query = $this->model::query();
        if (!is_null($relations)) {
            $query = $this->model::with($relations);
        }
        foreach ($criteria as $key => $value)
        {
            $query->where($key, $value);
        }
        if ($single) {
            return $query->first();
        }
        return $query->get();
    }

    public function paginate($perPage = 15, $page = 1)
    {
        return $this->model::paginate($perPage);
    }
}