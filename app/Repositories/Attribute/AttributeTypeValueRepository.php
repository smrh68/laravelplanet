<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 2/12/18
 * Time: 7:58 PM
 */

namespace App\Repositories\Attribute;


use App\Models\AttributeTypeValue;
use App\Repositories\Contract\BaseRepository;

class AttributeTypeValueRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = AttributeTypeValue::class;
    }
}