<?php

namespace App\Repositories\Attribute;


use App\Models\AttributeType;
use App\Repositories\Contract\BaseRepository;

class AttributeTypeRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct();
        $this->model = AttributeType::class;
    }
}