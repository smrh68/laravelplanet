<?php

namespace App\Presenters\Order;


use App\Models\Order\OrderStatus;
use App\Presenters\Contract\Presenter;

class OrderPresenter extends Presenter
{
    public function orderStatus()
    {
        $orderStatus = $this->entity->order_status;
        return OrderStatus::getOrderStatus($orderStatus);
    }

    public function orderStatusHtml()
    {
        $orderStatus = $this->entity->order_status;
        $orderStatusText = OrderStatus::getOrderStatus($orderStatus);
        $orderStatusClass = OrderStatus::getOrderStatusCssClass($orderStatus);
        return "<span class='badge {$orderStatusClass}'>{$orderStatusText}</span>";
    }

    public function operations()
    {
        $order = $this;
        return view('admin.order.operations', compact('order'));
    }

    public function isApproval()
    {
        return in_array($this->order_status, [OrderStatus::UNPAID,OrderStatus::CANCELED]);
    }
}