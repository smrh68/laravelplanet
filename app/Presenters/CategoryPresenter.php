<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 1/30/18
 * Time: 9:27 PM
 */

namespace App\Presenters;


use App\Presenters\Contract\Presenter;

class CategoryPresenter extends Presenter
{
    public function category_parent_title()
    {
        return isset($this->entity->parent) ? $this->entity->parent->category_title : '';
    }
}