<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 1/29/18
 * Time: 9:05 AM
 */

namespace App\Presenters;


use App\Presenters\Contract\Presenter;

class ProductPresenter extends Presenter
{
    public function product_price_in_hezar_toman()
    {
        return number_format($this->entity->product_price / 1000) . ' هزار تومان';
    }
}