<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 1/29/18
 * Time: 9:08 AM
 */

namespace App\Presenters\Contract;



trait Presentable
{
    protected $presenterInstance;

    public function present()
    {
        if (!$this->presenter || !class_exists($this->presenter)){
            throw new \Exception('presenter property not found.');
        }
        $this->presenterInstance = new $this->presenter($this);
        return $this->presenterInstance;
    }
}