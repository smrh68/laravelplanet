<?php
/**
 * Created by PhpStorm.
 * User: smrh
 * Date: 1/26/18
 * Time: 4:14 PM
 */

namespace App\Presenters;


class Currency
{
    public static function toman(int $amount)
    {
        return number_format($amount/10) . ' تومان';
    }
}